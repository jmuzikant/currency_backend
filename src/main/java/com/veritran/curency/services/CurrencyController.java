package com.veritran.curency.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class CurrencyController {

    @RequestMapping(value = "/")
    public ResponseEntity<String> getHealth() {
        return   new ResponseEntity<>("OK", HttpStatus.OK);
    }

    @RequestMapping(value = "/currencies")
    public ResponseEntity<Map<String,List<String>>> getCurrencies() {
        List<String> currencies = new ArrayList<String>();
        currencies.add("Dolar");
        currencies.add("Peso");
        currencies.add("Rublo");
        Map currenciesMap= new HashMap<>();
        currenciesMap.put("currencies",currencies);
        return   new ResponseEntity<>(currenciesMap, HttpStatus.OK);
    }
}
